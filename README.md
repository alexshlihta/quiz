### About

INTITA API portal created with [Laravel](https://laravel.com).

### Requirements

* PHP >= 7.0.0
* OpenSSL PHP Extension
* PDO PHP Extension
* Mbstring PHP Extension
* Tokenizer PHP Extension
* XML PHP Extension
* [Composer](https://getcomposer.org)

### Install

    $ git checkout <git-url> <project_path>
    $ cd <project_path>
    $ composer install
    $ cp .env.example .env
    $ php artisan key:generate
    
_Laravel installation instruction [here](https://laravel.com/docs/5.5#installation)_
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Middleware\CheckBlock;
use App\User;

class BlockedUser extends Model
{
    public function user(){
      return $this->belongsTo('App\User', 'user_id', 'id');
    }
    public function admin(){
      return $this->belongsTo('App\User', 'admin_id','id');
    }
}

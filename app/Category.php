<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function category(){
        return $this->belongsToMany('App\Quiz', 'category_to_quizzs','category_id', 'quiz_id');
    }
    public function quiz(){
        return $this->belongsTo('App\Quiz', 'quiz_id','id');
    }
    protected $fillable = ['name'];
}
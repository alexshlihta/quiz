<?php

namespace App\Http\Controllers\Admin;

use App\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\BlockedUser;

class BlockedUsersController extends Controller
{
    public function listblock(){
    	$users = User::with('blockedUser')->get()->toArray();
    	return view('admin.blockuser', compact('users'));
    }

    public function blockUser(Request $request){
    	$userId = $request->input('userId');
    	$user = User::find($userId);
    	$user->block($request->user());
    	return back();
    }

    public function unblockUser(Request $request){
    	$userId = $request->input('userId');
    	$user = User::find($userId);
    	$user->unblock();
    	return back();
    }

    public function errorMessage(){
        return view('admin.errorMessage');
    }     

}    
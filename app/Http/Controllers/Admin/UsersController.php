<?php

namespace App\Http\Controllers\Admin;

use App\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class UsersController extends Controller
{
    public function index()
    {
        $users = User::paginate(10);
        $roles = Role::all();
        $params = [
            'users' => $users,
            'roles' => $roles
        ];
        return view('admin.users.userlist', $params);
    }

    public function editRole(Request $request){
        $user_id=$request->input('userId');
        $role_id=$request->input('roleId');
        $user = User::find($user_id);
        $user->attachRole($role_id);
        return back();
    }
}

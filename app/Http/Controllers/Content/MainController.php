<?php

namespace App\Http\Controllers\Content;

use App\Http\Controllers\Controller;

class MainController extends Controller {
    public function __construct() {
    }

    public function mainPage() {
        return view('app');
    }

}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Quiz;
use Auth;
use Carbon;

class PassquizzeController extends Controller
{
    public function passquizze($id)
    {
        $user = Auth::user();
        $quiz = Quiz::find($id);
        if($user==null){
            $qaccess = $quiz->isPablic();
        }
        else {
            $qaccess = true;
        }
        if(Carbon\Carbon::now()->between(Carbon\Carbon::parse($quiz->start_ts), Carbon\Carbon::parse($quiz->end_ts))){
            $qtime = true;
        }
        else{
            $qtime = false;
        }
        $params = [
            'qaccess' => $qaccess,
            'qtime' => $qtime,
            'user' => $user,
            'quiz' => $quiz
        ];
        return view('quizzes.pass', $params);
    }
}
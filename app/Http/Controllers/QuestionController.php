<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use App\Quiz;
class QuestionController extends Controller
{
    public function questions_display(Request $request){
        $quizId = $request->input('quizId');
        $quizwrel = Quiz::where('id', $quizId)->with('questions')->first();
        $questions = $quizwrel->questions;
        $params = [
            'questions' => $questions,
            'quizwrel' => $quizwrel
        ];
        return view('quizzes.question_list', $params);
    }
    
    public function question_edit(Request $request){
        $quizId = $request->input('quizId');
        $quizwrel = Quiz::where('id', $quizId)->with('questions')->first();
        $questionid = $request->input('questionId');
        $question = Question::find($questionid);
        $params = [
            'questionid' => $questionid,
            'question' => $question,
            'quizwrel' => $quizwrel
        ];
        return view('quizzes.question_edit', $params);
    }
    
    public function save_question(Request $request){
        $question_id = $request->input('id');
        $question = Question::find($question_id);
        $question->question = $request->input('question');
        $question->save();
        $quizId = $request->input('quizId');
        $quizwrel = Quiz::where('id', $quizId)->with('questions')->first();
        $questions = $quizwrel->questions;
        $params = [
            'questions' => $questions,
            'questionid' => $question_id,
            'question' => $question,
            'quizwrel' => $quizwrel
        ];
        return view('quizzes.question_list', $params);
    }
    
    public function add_question(Request $request){
        $quizId = $request->input('quizId');
        $quizwrel = Quiz::where('id', $quizId)->with('questions')->first();
        $params = [
            'quizwrel' => $quizwrel
        ];
        return view('quizzes.question_add', $params);
    }
    
    public function save_new(Request $request){
        $quizId = $request->input('quizId');
        $new_question = new Question();
        $new_question->question = $request->input('question');
        if (!$new_question->hasQuestion($new_question->question)) {
            $new_question->save();
            $quiz = Quiz::where('id', $quizId)->with('questions')->first();
            $quiz->attachQuestion($new_question);
        }
        $quizwrel = Quiz::where('id', $quizId)->with('questions')->first();
        $questions = $quizwrel->questions;
        $params = [
            'questions' => $questions,
            'quizwrel' => $quizwrel
        ];
        return view('quizzes.question_list', $params);
    }
    
    public function delete_question(Request $request){
        $questionid = $request->input('questionId');
        $question = Question::find($questionid);
        $question->delete();
        $quizId = $request->input('quizId');
        $quizwrel = Quiz::where('id', $quizId)->with('questions')->first();

        $questions = $quizwrel->questions;
        $params = [
            'questions' => $questions,
            'question' => $question,
            'quizwrel' => $quizwrel
        ];
        return view('quizzes.question_list', $params);
    }
}
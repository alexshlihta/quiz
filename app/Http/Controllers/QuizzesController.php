<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Quiz;
use App\Category;
class QuizzesController extends Controller
{
    public function quizzesDisplay(){
        $quizzes = Quiz::with('category')->get();
        return view('quizzes.quizzes_display', compact('quizzes'));
    }
    
    public function validateData(Request $request){
        return $request->validate([
            'name' => 'required|max:255',
            'duration' => 'required|integer',
            'start_ts' => 'required|date',
            'end_ts' => 'required|date|after:start_ts',
            'public' => 'required'
        ]);
    }
    
    public function store(Request $request)
    {
        self::validateData($request);
        $data = new Quiz();
        $data->name = $request->name;
        $data->duration = $request->duration;
        $data->start_ts = $request->start_ts;
        $data->end_ts = $request->end_ts;
        $data->public = $request->public;
        $data->save();
        self::attachDetachCategory($request, $data->id);
        return redirect('quizzes');
    }
    
    public function create(){
        $category = Category::all();
        return view('quizzes.quiz_create', compact('category'));
    }
    
    public function delete ($id){
        $quiz  = Quiz::find($id);
        $quiz->category()->detach();
        $quiz -> delete();
        return redirect('quizzes');
    }
    
    public function edit ($id){
        $quiz  = Quiz::find($id);
        return view('quizzes.quiz_create', compact('quiz', 'id'), ['category' => Category::all()]);
    }
    
    public function update ($id, Request $request){
        
        self::validateData($request);
        $quiz = Quiz::find($id);
        $quiz->name = $request->name;
        $quiz->duration = $request->duration;
        $quiz->start_ts = $request->start_ts;
        $quiz->end_ts = $request->end_ts;
        $quiz->public = $request->public;
        $quiz->save();
        self::attachDetachCategory($request, $id);
        return redirect('quizzes');
    }
    
    public function categoryToQuizz ($category_id, $quiz_id)
    {
        $quiz = Quiz::find($quiz_id);
        $quiz->categoryToQuizz($category_id, $quiz_id);
        return back();
    }
    
    public function attachDetachCategory($request, $id){
        $quiz = Quiz::find($id);
        $category = $request->input('category');
        $quiz->attachDetachCategory($category);
    }
    
    
    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    
    
}

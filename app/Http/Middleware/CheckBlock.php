<?php

namespace App\Http\Middleware;

use Closure;
use App\BlockedUser;
use Auth;
use App\User;
class CheckBlock
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
   /* public function boot(Router $router){
        p
    }*/

    public function handle($request, Closure $next)
    {
        $user = $request->user();
        if($request->url() !=='/blockMessage'){
            if(isset($user)){
                if($user->isBlocked())
                {
                    Auth::logout();
                    return redirect('/blockMessage');
                }
            }
        }
        return $next($request);
    }
}

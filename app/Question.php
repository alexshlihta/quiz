<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model {
    public function quizzes(){
        return $this->hasOne('App\Quiz','question_id','quiz_id');
    }
    
    public function hasQuestion($question){
        if ($this->where('question', $question)->first()){
            return true;
        }
        return false;
    }
}
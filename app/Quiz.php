<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{
    public function questions(){
        return $this->belongsToMany('App\Question', 'quiz_question', 'quiz_id', 'question_id');
    }

    public function isPablic(){
        if ($this->public){
            return true;
        }
        return false;
    }
    public function quiz(){
        return $this->hasOne('App\Category','quiz_id','id');
    }
    public function category(){
        return $this->belongsToMany('App\Category', 'category_to_quizzs','quiz_id', 'category_id' );
    }

    public function chooseCategory($category_id, $quiz_id){
        $category_quiz = new Category;
        $category_quiz->quiz_id = $quiz_id;
        $category_quiz->category_id = $category_id;
        $category_quiz->save();
    }

    public function hasCategory($category_id){
        if ($this->category()->where('category_id', $category_id)->first()){
            return true;
        }
        return false;
    }

    public function attachDetachCategory($category){
        $this->category()->sync($category);
    }

    public function attachQuestion($question){
        $this->questions($question)->save($question);
    }
}

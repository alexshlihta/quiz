<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
        return $this->belongsToMany('App\Role', 'users_roles', 'user_id', 'role_id');
    }

    public function rolesList(){
        return $this->roles->implode('display_name', ', ');
    }

    public function attachRole($role) {
        $this->roles()->detach();
        if(!$this->hasRole($role)){
            $this->roles()->attach($role);
        }
    }

    public function hasRole($role){
        if ($this->roles()->where('id', $role)->first()){
            return true;
        }
        return false;
    }

    public function isAdmin(){
        return $this->hasRole(1);
    }

    public function blockedUser(){
      return $this->hasOne('App\BlockedUser','user_id','id');
    }

    public function block($admin) {
        $blockedUser = new BlockedUser();
        $blockedUser->user_id=$this->id;
        $blockedUser->admin_id=$admin->id;
        if($blockedUser->user_id !== $blockedUser->admin_id){
            $blockedUser->save();
        }
    }

    public function unblock() {
        $this->blockedUser->delete();
    }

    public function isBlocked(){
        $blockedUser = $this->blockedUser;
        if(isset($blockedUser)){
            return true;
        }

    }
}

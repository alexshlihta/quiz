<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert ([
            ['name' => 'DMAPA'],
            ['name' => 'Java'],
            ['name' => 'PHP'],
            ['name' => 'JS'],
            ['name' => 'DB']
        ]);
    }
}

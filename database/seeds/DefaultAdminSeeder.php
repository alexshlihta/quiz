<?php

use Illuminate\Database\Seeder;
use App\User;
class DefaultAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	
    	$role_admin=DB::table('roles')->where('name', 'admin')->first();
        $user = new User();
        $user->name='admin';
        $user->email='admin@gmail.com';
        $user->password=bcrypt('123456');
        $user->save();
        $user->roles()->attach($role_admin->id);
    }
}

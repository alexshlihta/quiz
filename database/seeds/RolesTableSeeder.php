<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert ([
            ['name' => 'admin', 'display_name' => 'Адміністратор'],
            ['name' => 'user', 'display_name' => 'Користувач'],
            ['name' => 'teacher', 'display_name' => 'Викладач'],
            ['name' => 'student', 'display_name' => 'Студент'],
        ]);
    }
}

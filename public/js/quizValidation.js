$(document).ready(function(){
    jVal = {
        'quizzName' : function() {
            $('body').append('<div id="nameInfo" class="info"></div>');
            var nameInfo = $('#nameInfo');
            var elem = $('#name');
            var pos = elem.offset();
            nameInfo.css({
                top: pos.top-3,
                left: pos.left +elem.width()+15
            });
            if(elem.val().length < 3) {
                error.name = true;
                nameInfo.removeClass('correct').addClass('error').html('← At least 3 characters').show();
                elem.removeClass('normal').addClass('wrong');
            } else {
                error.name = false;
                nameInfo.removeClass('error').addClass('correct').html('√').show();
                elem.removeClass('wrong').addClass('normal');
            }
        },
        'duration' : function (){
            $('body').append('<div id="duratInfo" class="info"></div>');
            var duratInfo = $('#duratInfo');
            var elem = $('#duration');
            var pos = elem.offset();
            duratInfo.css({
                top: pos.top-3,
                left: pos.left+elem.width()+15
            });
            var patt = /^\d+$/i;
            if(!patt.test($('#duration').val()) ){
                error.duration = true;
                duratInfo.removeClass('correct').addClass('error').html('← Input the integer value').show();
                elem.removeClass('normal').addClass('wrong');
            } else {
                error.duration = false;
                duratInfo.removeClass('error').addClass('correct').html('√').show();
                elem.removeClass('wrong').addClass('normal');
            }
        },
        'start_ts' : function (){
            $('body').append('<div id="start_tsInfo" class="info"></div>');
            var start_tsInfo = $('#start_tsInfo');
            var elem = $('#start_ts');
            var pos = elem.offset();
            start_tsInfo.css({
                top: pos.top-3,
                left: pos.left+elem.width()+15
            });
            var patt = /^20\d{2}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/i;
            if(!patt.test($('#start_ts').val())) {
                error.start = true;
                start_tsInfo.removeClass('correct').addClass('error').html('← Input date in correct format').show();
                elem.removeClass('normal').addClass('wrong');
            } else {
                error.start = false;
                start_tsInfo.removeClass('error').addClass('correct').html('√').show();
                elem.removeClass('wrong').addClass('normal');
            }
        },
        'end_ts' : function (){
            $('body').append('<div id="end_tsInfo" class="info"></div>');
            var end_tsInfo = $('#end_tsInfo');
            var elem = $('#end_ts');
            var pos = elem.offset();
            end_tsInfo.css({
                top: pos.top-3,
                left: pos.left+elem.width()+15
            });
            var patt = /^20\d{2}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/i;
            if(!patt.test($('#end_ts').val())) {
                error.end = true;
                end_tsInfo.removeClass('correct').addClass('error').html('← Input date in correct format').show();
                elem.removeClass('normal').addClass('wrong');
            } else {
                error.end = false;
                end_tsInfo.removeClass('error').addClass('correct').html('√').show();
                elem.removeClass('wrong').addClass('normal');
            }},
            'endValid' : function () {
                $('body').append('<div id="endValidInfo" class="info"></div>');
                var endValidInfo = $('#endValidInfo');
                var elem = $('#end_ts');
                var pos = elem.offset();
                endValidInfo.css({
                    top: pos.top - 3,
                    left: pos.left + elem.width() + 15
                });
                var patt = /^20\d{2}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/i;
                if (Date.parse($('#end_ts').val()) < Date.parse($('#start_ts').val())) {
                    error.endVal = true;
                    endValidInfo.removeClass('correct').addClass('error').html('← Date should be after \"Available from\".').show();
                    elem.removeClass('normal').addClass('wrong');
                } else {
                    error.endVal = false;
                    endValidInfo.removeClass('error').addClass('correct').html('√').show();
                    elem.removeClass('wrong').addClass('normal');
                }
            }
    };
    $('#name').change(jVal.quizzName);
    $('#duration').change(jVal.duration);
    $('#start_ts').change(jVal.start_ts);
    $('#end_ts').change(jVal.endValid);
    $('#end_ts').change(jVal.end_ts);

    var error = {
        name: true,
        duration: true,
        start: true,
        end: true,
        endVal: true
    };
    function checkButton(){
        if(!error.name && !error.duration && !error.start && !error.end && !error.endVal){
            $(":submit").removeAttr("disabled");
        }
        else{
            $(":submit").attr("disabled", true);
        }
    }
    document.getElementById("name").onblur = checkButton;
    document.getElementById("duration").onblur = checkButton;
    document.getElementById("start_ts").onblur = checkButton;
    document.getElementById("end_ts").onblur = checkButton;
});


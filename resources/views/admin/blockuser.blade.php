@extends('admin.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="top mx-auto top">
                <h1 class="text-center">Users List</h1>
                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Active users</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Blocked users</a>
                    </li>
                </ul>
<div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
        <table class="table table-bordered text-center">
            <thead class="thead-light">
            <tr>
                <th>Id</th>
                <th>Nickname</th>
                <th>Email</th>
                <th>Block</th>
            </thead>
            </tr>
            @foreach($users as $user)
            @if($user['blocked_user']===null)
                <tr>
                    <td>{{$user['id']}}</td>
                    <td>{{$user['name']}}</td>
                    <td>{{$user['email']}}</td>
                    <td>
                        <form method="post" action="{{ route('admin.user.block') }}">
                            <input type="hidden" name="userId" value="{{$user['id']}}">
                            <button type="submit" class="btn btn-primary btn-sm">Block</button>
                            {{ csrf_field() }}
                        </form>
                    </td>
                </tr>
                @endif
            @endforeach
        </table></div>
  <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
        <table class="table table-bordered text-center">
            <thead class="thead-light">
            <tr>
                <th>Id</th>
                <th>Nickname</th>
                <th>Email</th>
                <th>Id of admin</th>
                <th>Unblock</th>
            </thead>
            </tr>
            @for($i=0; $i <count($users); $i++)
            @if($users[$i]['blocked_user']!==null)
                <tr>
                    <td>{{$users[$i]['id']}}</td>
                    <td>{{$users[$i]['name']}}</td>
                    <td>{{$users[$i]['email']}}</td>
                    <td>{{$users[$i]['blocked_user']['admin_id']}}</td>
                    <td>
                        <form method="post" action="{{ route('admin.user.unblock') }}">
                            <input type="hidden" name="userId" value="{{$users[$i]['id']}}">
                            <button type="submit" class="btn btn-primary btn-sm">Unblock</button>
                            {{ csrf_field() }}
                        </form>
                    </td>
                </tr>
                @endif
            @endfor
            </table>
        </div>
    </div>
</div>
</div>
</div>
@endsection
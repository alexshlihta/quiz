@extends('app')

@section('content')
<div class="container">

     <div class="jumbotron">
     	<div class="text-center">
        <h1>You have been banned from this server!</h1>
        <p class="lead">Please contact administrator.</p>
        <p class="lead">admin@gmail.com</p>
      	</div>
	</div>
</div>
@endsection    
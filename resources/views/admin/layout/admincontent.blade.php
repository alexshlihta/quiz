<div class="container-fluid">
    <div class="row">
        <nav class="col-sm-2">
            @include('admin.layout.navbar')
        </nav>
        <div class="col-sm-10 mx-auto">
            @yield('content')
        </div>
    </div>
</div>

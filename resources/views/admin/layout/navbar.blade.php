<ul class="nav flex-column mt-5 nav-pills sticky-top">
    <li class="nav-item">
        <a class="nav-link {{ Request::is('admin') ? 'active' : '' }}" href="{{ route('admin.index') }}">Home</a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{ Request::is('admin/userlist') ? 'active' : '' }}" href="{{ route('admin.users.userlist') }}">User's List</a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{ Request::is('admin/userblock') ? 'active' : '' }}" href="{{ route('admin.users.list') }}">Block/Unblock User's</a>
    </li>
</ul>
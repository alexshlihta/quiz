@extends('admin.app')

@section('content')
        <h1 class="text-center">Users List</h1>
        <table class="table table-bordered text-center">
            <thead class="thead-light">
            <tr>
                <th>#</th>
                <th>Nickname</th>
                <th>Email</th>
                <th>Admin</th>
                <th>User</th>
                <th>Teacher</th>
                <th>Student</th>
                <th>Edit role</th>
            </tr>
            </thead>
            @foreach($users as $user)
                <tr>
                    <td>{{$user->id}}</td>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <form method="post" action="{{ action('Admin\UsersController@editRole') }}">
                        <input type="hidden" name="userId" value="{{$user->id}}">
                        @foreach ($roles as $role)
                            <td><input type="checkbox" name="roleId[{{$loop->index}}]" value="{{$role->id}}" {{$user->hasRole($role->id) ? 'checked' : ''}}></td>
                        @endforeach
                        <td><button type="sumbit" class="btn btn-primary btn-sm">Save role</button></td>
                        {{ csrf_field() }}
                    </form>
                </tr>
                @endforeach
        </table>
        {{ $users->links( "pagination::bootstrap-4") }}
@endsection
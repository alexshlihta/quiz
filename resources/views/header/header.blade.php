<nav class="navbar navbar-expand-lg navbar-light bg-light ">
    <a href="/">
        <img class="img-fluid" src="{{ asset('img/logo.jpg') }}">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
            aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav ">
            <li class="nav-item">
                <a class="nav-link" href="/quizzes">Quizzes</a>
            </li>
            @can('admin_section')
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.index') }}">Admin Panel</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('categories') }}">Categories</a>
                </li>
            @endcan
        </ul>
        @guest
            <ul class="navbar-nav ml-auto">
                <li class="nav-item ">
                    <a class="nav-link" href="{{ route('login') }}">Log In</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="{{ route('register') }}">Sign Up</a>
                </li>
            </ul>
        @else
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown ">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown">
                        {{ Auth::user()->name }}
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <form action="{{route('profile.index')}}" method="GET">
                            {{ csrf_field() }}
                            <button class="dropdown-item text-center" type="submit">Profile</button>
                        </form>
                        <form action="{{route('logout')}}" method="POST">
                            {{ csrf_field() }}
                            <button class="dropdown-item text-center" type="submit">Log Out</button>
                        </form>
                    </div>
                </li>
            </ul>
        @endguest
    </div>
</nav>

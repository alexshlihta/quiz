@extends('app')
@section('content')
    <div class="page-header">
        <h1>{{Auth::user()->name}} profile</h1>
    </div>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>name</th>
            <th>email</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>{{Auth::user()->name}}</td>
            <td>{{Auth::user()->email}}</td>
        </tr>
        </tbody>
    </table>
@endsection
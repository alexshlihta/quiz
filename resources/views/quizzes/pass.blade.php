<!doctype html>
<html lang="uk">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Quizzes</title>
    @section('script')
        @include('layout.common_scripts')
    @show
    @section('style')
        @include('layout.common_styles')
    @show
</head>
<body>
    <div class="container">
        <div class="card m-1">
            <div class="card-body">
                @if ($user != null && $qtime)
                    <p class="card-text text-left">Hello {{$user->name}}</p>
                    <p class="card-text text-left">Email: {{$user->email}}</p>
                @elseif ($qaccess) <p class="card-text">Hello guest</p>
                @endif
            </div>
        </div>
        @if(!$qaccess)
            <script>alert("Quiz is not publick! Log in!")</script>
            <script>window.location = "/login"</script>
        @else
            @if (!$qtime)
                <script>alert("Quiz is end!")</script>
                <script>window.location = "/quizzes"</script>
            @endif
            PLACEHOLDER FOR QUIZ
        @endif
    </div>
</body>
</html>
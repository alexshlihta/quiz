@extends('app')

@section('content')
    <div class="container col-sm-7">
        <h1 class="text-center"> Questions </h1>
        <form action="{{action('QuestionController@save_new')}}" method="POST">
            {{csrf_field()}}
            <div class="form-group">
                <label for="Question">Name </label>
                <input type="hidden" name="quizId" value="{{$quizwrel->id}}">
                <input type="text" class="form-control" name="question" value="">
                <input type="hidden" name="id" value="">
            </div>
            <td>
                <button type="submit" class="btn btn-outline-success btn-lg btn-block">Save new question</button>
            </td>
        </form>
    </div>
@endsection
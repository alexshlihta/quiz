@extends('app')

@section('content')
    <div class="container col-sm-7">
        <h1 class="text-center">Edit qeustion</h1>
        <form action=" {{action('QuestionController@save_question')}}" method="POST">
            {{csrf_field()}}
            <div class="form-group">
                <label for="Question">Name</label>
                <input type="text" class="form-control" name="question" value="{{$question->question}}">
                <input type="hidden" name="quizId" value="{{$quizwrel->id}}">
                <input type="hidden" name="id" value="{{$question->id}}">
            </div>
            <td>
                <button type="submit" class="btn btn-outline-success btn-lg btn-block">Save question/Go back</button>
            </td>
        </form>
    </div>
@endsection
@extends('app')

@section('content')
    <div class="container">
        <h1 class="text-center">{{$quizwrel->name}}</h1>
        <table class="table table-hover">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Question</th>
                <th scope="col">Edit</th>
                <th scope="col">Delete</th>
            </tr>
            </thead>
            <tbody>
            @foreach($questions as $question)
                <tr>
                    <td>{{$question->id}}</td>
                    <td>{{$question->question}}</td>
                    <td>
                        <form method="POST" action="{{ action('QuestionController@question_edit') }}">
                            <input type="hidden" name="questionId" value="{{$question->id}}">
                            <input type="hidden" name="quizId" value="{{$quizwrel->id}}">
                            <button type="sumbit" class="btn btn-outline-danger btn-sm">Edit question</button>
                        {{ csrf_field() }}
                        </form>
                    </td>
                    <td>
                    <form method="POST" action="{{ action('QuestionController@delete_question') }}">
                            <input type="hidden" name="questionId" value="{{$question->id}}">
                            <input type="hidden" name="quizId" value="{{$quizwrel->id}}">
                            <button type="sumbit" class="btn btn-outline-danger btn-sm">Delete question</button>
                            {{ csrf_field() }}
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <form method="get" action="{{ action('QuestionController@add_question') }}">
            <input type="hidden" name="quizId" value="{{$quizwrel->id}}">
            <button type="sumbit" class="btn btn-outline-primary btn-lg btn-block">Add new</button>
            {{ csrf_field() }}
        </form><br>
        <a href="/quizzes" class="btn btn-outline-primary btn-lg btn-block">Back</a>
    </div>
@endsection

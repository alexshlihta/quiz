@extends('app')

@php
    if (isset($quiz)){
        $url = action('QuizzesController@update', $quiz->id);
        $typeDate = "datetime";
        $nameButton = " Save ";
        $namePage = "Edit the data";
        $placeholderName = '';
        $placeholderDuration = '';
        $classButton = "btn btn-outline-success btn-lg btn-block";
    } else {
        $url = url('quizzes/create');
        $typeDate = "date";
        $nameButton = " Create ";
        $namePage = "Input the data";
        $placeholderName = "Input name of the test.";
        $placeholderDuration = "Input duration of the test.";
        $classButton = "btn btn-outline-primary btn-lg btn-block";
    }
@endphp
@section('script')
    @parent
    @include('layout.scriptsValidate')
@endsection()
@section('style')
    @parent
    @include('layout.styleValidate')
@endsection


@section('content')
    <div class="container col-sm-7">
        <h1 class="text-center"> {{ $namePage }} </h1>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
            <form action="{{ $url }}" method="POST">
             {{csrf_field()}}


                <div class="form-group row">
                    <label for="end_date" class="col-sm-3 col-form-label">Categories</label>
                    <div class="col-sm-8">
                        <table>
                            <tr>
                                @foreach ($category as $category)
                                    <td>
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label" for="{{ $category->id }}">
                                                @if(isset($quiz))
                                                    <input class="form-check-input" type="checkbox"
                                                           id="{{ $category->id }}"
                                                           value="{{ $category->id }}"
                                                           name="category[{{$loop->index}}]"
                                                            {{ $quiz->hasCategory($category->id) ? 'checked' : ''}}>
                                                @else
                                                    <input class="form-check-input"
                                                           type="checkbox"
                                                           id="{{ $category->id }}"
                                                           name="category[{{$loop->index}}]"
                                                           value="{{ $category->id }}">
                                                @endif
                                                {{ $category->name }}</label>
                                        </div>
                                    </td>
                                @endforeach
                            </tr>
                        </table>
                    </div>
                </div>

                <div class="form-group">
                    <label for="Quizzes">Name </label>
                    <input type="text" class="form-control" rows="1" name="name" id="name"
                           value="{{ isset($quiz->name) ? $quiz->name:null }}"
                            placeholder="{{ $placeholderName }}"> </input>
                </div>

                <div class="form-group">
                    <label for="Quizzes">Test duration </label>
                    <input type="number" class="form-control" aria-rowspan="1" name="duration" id="duration"
                           value="{{ isset($quiz->duration) ? $quiz->duration:null }}"
                            placeholder="{{ $placeholderDuration }}"> </input>
                </div>

                <div class=form-group">
                    <label for="Quizzes">Available from </label>
                    <input type="{{ $typeDate }}" class="form-control" id="start_ts" rows="1" name="start_ts"
                           value="{{ isset($quiz->start_ts) ? $quiz->start_ts:null }}"> </input>
                </div>
                <div class="form-group">
                    <label for="Quizzes">Available to</label>
                    <input type="{{ $typeDate }}" class="form-control" id="end_ts" rows="1" name="end_ts"
                           value="{{ isset($quiz->end_ts) ? $quiz->end_ts:null }}"> </input>
                </div>
                <button type="submit" class="{{ $classButton }}">{{ $nameButton }}</button>
            </form>
    </div>
@endsection

@extends('app')

@section('content')
    <div class="container">
        <h1 class="text-center">Quizzes</h1>
        <table class="table table-hover">
            <thead>
            <tr>
                <th scope="col">Name</th>
                <th scope="col">Test duration</th>
                <th scope="col">Available from</th>
                <th scope="col">Available to</th>
                <th scope="col">Category</th>
                <th scope="col"></th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            @foreach($quizzes as $quiz)

                <tr>
                    <td>{{$quiz->name}}</td>
                    <td>{{$quiz->duration}}</td>
                    <td>
                        {{$quiz->start_ts}}
                    </td>
                    <td>
                        {{$quiz->end_ts}}
                    </td>

                    <td>@foreach($quiz->category as $category){{$category->name}}<br>@endforeach</td>

                    <td>
                        <a href="{{ route('quiz.edit', ['id' => $quiz->id])}}" class="btn btn-outline-success btn-sm">Edit</a>
                    </td>

                    <td>
                        <form action="{{route('quiz.delete', ['id'=>$quiz->id])}}" method="POST">
                            {{csrf_field()}}
                            {{method_field('DELETE')}}
                            <button class="btn btn-outline-danger btn-sm">Delete</button>
                        </form>
                    </td>
                    <td>
                        <a href="{{route('quiz.pass', ['id'=>$quiz->id])}}" class="btn btn-outline-success btn-sm btn-block" alt="Placeholder">Pass the quizze</a>
                    </td>
                    <td>
                        <form method="post" action="{{ action('QuestionController@questions_display') }}">
                            <input type="hidden" name="quizId" value="{{$quiz->id}}">
                            <button type="sumbit" class="btn btn-outline-danger btn-sm">Add/Edit questions</button></td>
                        {{ csrf_field() }}
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <a href="{{route('quiz.create')}}" class="btn btn-outline-primary btn-lg btn-block">Create</a>
    </div>

@endsection

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Content\MainController@mainPage');
Route::group(['prefix' => 'admin', 'middleware' => 'can:admin_section'], function () {
    Route::get('/', 'AdminController@index')->name('admin.index');
    Route::get('/userlist', 'Admin\UsersController@index')->name('admin.users.userlist');
    Route::post('/userlist', 'Admin\UsersController@editRole');
    Route::post('/user/unblock', 'Admin\BlockedUsersController@unblockUser')->name('admin.user.unblock');
    Route::get ('/userblock', 'Admin\BlockedUsersController@listblock')->name('admin.users.list');
    Route::post('/user/block', 'Admin\BlockedUsersController@blockUser')->name('admin.user.block');
});
Route::get('/profile', 'ProfileController@index',['middleware' => 'auth'])->name('profile.index');
Auth::routes();
Route::group(['prefix' => '/quizzes'], function () {
    Route::get('/', 'QuizzesController@quizzesDisplay');
    Route::get('/create', 'QuizzesController@create');
    Route::post('/create', 'QuizzesController@store')->name('quiz.create');
    Route::delete('/{id}', 'QuizzesController@delete')->name('quiz.delete');
    Route::get('/{id}/edit', 'QuizzesController@edit')->name('quiz.edit');
    Route::post('/{id}', 'QuizzesController@update')->name('quiz.update');
    Route::get('/{id}/pass', 'PassquizzeController@passquizze')->name('quiz.pass');
});
Route::group(['prefix' => '/question'], function () {
    Route::match(['get', 'post'],'/list','QuestionController@questions_display')->name('question.list');
    Route::match(['get', 'post'],'/edit','QuestionController@question_edit')->name('question.edit');
    Route::match(['get', 'post'],'/','QuestionController@delete_question')->name('question.delete');
    Route::post('/save', 'QuestionController@save_question');
    Route::get('/add', 'QuestionController@add_question')->name('add.question');
    Route::post('/savenew', 'QuestionController@save_new');
});
Auth::routes();
Route::get('/blockMessage', 'Admin\BlockedUsersController@errorMessage')->name('blockMessage');
Route::group(['prefix' => 'categories', 'middleware' => 'can:admin_section'], function () {
    Route::get('/', 'CategoriesController@index')->name('categories');
    Route::post('/create', 'CategoriesController@store')->name('category.create');
    Route::delete('/{id}', 'CategoriesController@destroy')->name('category.delete');
});


